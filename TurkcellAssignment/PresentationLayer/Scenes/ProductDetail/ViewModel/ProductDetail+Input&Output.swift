//
//  ProductDetail+Input&Output.swift
//  TurkcellAssignment
//
//  Created by Abdullah Okudan on 15.03.2022.
//

import Foundation
protocol ProductDetailInput: AnyObject {
    func viewDidLoad()
}

protocol ProductDetailOutput: AnyObject {
    func customise()
    func refresh()
}
