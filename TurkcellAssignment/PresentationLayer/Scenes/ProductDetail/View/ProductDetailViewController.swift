//
//  ProductDetailViewController.swift
//  TurkcellAssignment
//
//  Created by Abdullah Okudan on 15.03.2022.
//

import Foundation
import UIKit

class ProductDetailViewController: BaseViewController {

    let viewModel: ProductDetailViewModel
    
    lazy var scrll: UIScrollView = {
        let sc = UIScrollView(frame: .zero)
        sc.translatesAutoresizingMaskIntoConstraints = false
        return sc
    }()
    
    lazy var contentView: UIView = {
        let cv = UIView(frame: .zero)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 8
        stackView.alignment = .leading
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    lazy var imgProduct: DownloadableImageView = {
        let img = DownloadableImageView(frame: .zero)
        return img
    }()
    
    lazy var lblTitle: H1HeaderLabel = {
        let lbl = H1HeaderLabel(frame: .zero)
        return lbl
    }()
    
    lazy var lblDescription: P1TextLabel = {
        let lbl = P1TextLabel(frame: .zero)
        return lbl
    }()

    
    required init(viewModel: ProductDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.output = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.input?.viewDidLoad()
    }
}

extension ProductDetailViewController {

}

extension ProductDetailViewController: ProductDetailOutput {
    func customise(){
        self.view.backgroundColor = .white
        
        self.view.addSubview(scrll)
        NSLayoutConstraint.activate([
            scrll.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            scrll.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            scrll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrll.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
        
        self.scrll.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: self.scrll.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: self.scrll.trailingAnchor, constant: 0),
            contentView.topAnchor.constraint(equalTo: self.scrll.topAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: self.scrll.bottomAnchor, constant: 0),
            contentView.widthAnchor.constraint(equalTo: scrll.widthAnchor)
        ])
        
        self.contentView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -10),
            stackView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0)
        ])
        
        imgProduct.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        stackView.addArrangedSubview(imgProduct)
        stackView.addArrangedSubview(lblTitle)
        stackView.addArrangedSubview(lblDescription)
    }
    
    func refresh() {
        self.navigationItem.title = "Product Detail"
        lblTitle.text = viewModel.productDetail.name ?? ""
        lblDescription.text = viewModel.productDetail.descriptionProduct ?? ""
        imgProduct.downloadWithUrlSession(with: viewModel.productDetail.image)
    }
}

extension ProductDetailViewController: BaseOutput {
    func didUpdateState() {
        switch viewModel.state {
        case .idle:
            removeLoadingIndicator()
        case .loading:
            showLoadingIndicator(onView: self.view)
        case .error(let message):
            removeLoadingIndicator()
            showAlertMessage(message: message)
        case .success( _):
            break
        }
    }
}


