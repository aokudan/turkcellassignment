# TurkcellAssignment

Turkcell interview

## Screenshots
<p align="center">
  <img src="screen1.png" width="350" title="Products Screen">
  <img src="screen2.png" width="350" alt="Product Detail Screen">
</p>

## Let’s Start
This project is designed on MVVM C architecture. The project consists of 4 layers.

## 1) Application Layer
Contains the AppDelegate.swift, AppDependency.swift and AppCoordinator.swift files, which is responsible for setting up the initial view controller of our app


## 2) Presentation Layer 
Contains view controllers, view models, and their coordinators. It has two scenes: Products (displays products in a UICollectionView) and ProductDetail (shows a product detail that the user selects on the Products scene). It consists of 3 subsections;

	1. Coordinator: Coordinator abstraction definition has been made.
	2. Wireframe: Transitions between screens in the application are made through this class.
	3. Scenes: Screens within the app. There are Products and ProductDetail screens.


## 3) Business Logic Layer 
Consists of a model and services. We use services to implement a certain business logic — e.g., fetching a list of Products. It consists of 2 subsections;

	1. Services: Rest service definitions are made here. 
	2. Models: Rest Service and Core Data models are available here.


## 4)Core Layer
Defines all of the settings we need for our Business Logic Layer to function and other small utilities. It consists of 7 subsections;

	1. Design: Issues related to the design of the application are determined here. Elements such as different labels and buttons are kept here.
	2. Network: The necessary infrastructure for web service calls is here.
	3. CoreData: Database connections are executed here.
	4. Cache: Keeping images in cache
	5. Operations: Operations such as image downloading.
	6. Utilities: Definitions used in the application.
	7. Extensions: Class extensions


## Supporting Files
Assets and Launch Screen


## TurkcellAssignmentTests
Unit Tests and UI Tests

